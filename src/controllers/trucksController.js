require('dotenv').config();
const jwt = require('jsonwebtoken');
const { truckJoiSchema } = require('../models/Truck');
const {
  saveTruck, findTrucks, findTruck, putTruck, delTruck, isHasTruck, assignTruckTo,
} = require('../services/trucksService');

// eslint-disable-next-line consistent-return
async function createTruck(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { type } = req.body;
    // eslint-disable-next-line camelcase
    const created_by = tokenPayload.userId;
    await truckJoiSchema.validateAsync({ type });
    // eslint-disable-next-line no-unused-vars,camelcase
    const truck = await saveTruck({ created_by, type });
    return await res.status(200).json({ message: 'Truck created successfully' });
  } catch (err) {
    next(err);
  }
}

// eslint-disable-next-line consistent-return
async function getTrucks(req, res, next) {
  const { authorization } = req.headers;

  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const id = tokenPayload.userId;
    findTrucks(id).then((result) => {
      res.status(200).json({ trucks: result });
    });
  } catch (err) {
    next(err);
  }
}

// eslint-disable-next-line consistent-return
async function getTruck(req, res, next) {
  console.log(req.headers.authorization);
  const { authorization } = req.headers;

  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    // eslint-disable-next-line no-unused-vars
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { id } = req.params;
    console.log(id);
    findTruck(id).then((result) => {
      res.status(200).json({ truck: result });
    });
  } catch (err) {
    next(err);
  }
}

// eslint-disable-next-line consistent-return
async function updateTruck(req, res, next) {
  const { authorization } = req.headers;

  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    // eslint-disable-next-line no-unused-vars
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { id } = req.params;
    const { type } = req.body;
    putTruck(id, type).then(() => {
      res.status(200).json({ truck: 'Truck details changed successfully' });
    });
  } catch (err) {
    next(err);
  }
}

// eslint-disable-next-line consistent-return
async function deleteTruck(req, res, next) {
  const { authorization } = req.headers;

  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    // eslint-disable-next-line no-unused-vars
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { id } = req.params;
    delTruck(id).then(() => {
      res.status(200).json({ truck: 'Truck deleted successfully' });
    });
  } catch (err) {
    next(err);
  }
}

// eslint-disable-next-line consistent-return
async function assignTruck(req, res, next) {
  const { authorization } = req.headers;

  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { id } = req.params;
    const { userId } = tokenPayload;
    isHasTruck(userId).then((data) => {
      if (data) {
        res.status(400).json({ message: 'Driver already has assigned truck' });
      } else {
        assignTruckTo(id, userId).then(() => {
          res.status(200).json({ message: 'Truck assigned successfully' });
        });
      }
    });
  } catch (err) {
    next(err);
  }
}

module.exports = {
  createTruck,
  getTrucks,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
};
