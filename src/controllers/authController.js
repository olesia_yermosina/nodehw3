require('dotenv').config();
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User, userJoiSchema } = require('../models/User');
const { saveUser } = require('../services/authService');
const { sendPassword } = require('../services/mailService');

// eslint-disable-next-line no-unused-vars
const registerUser = async (req, res, next) => {
  const { email, password, role } = req.body;
  await userJoiSchema.validateAsync({ email, password, role });

  // eslint-disable-next-line no-unused-vars
  const user = await saveUser({ email, password, role });
  // eslint-disable-next-line no-return-await
  return await res.status(200).json({ message: 'Profile created successfully' });
};

// eslint-disable-next-line no-unused-vars
const loginUser = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    // eslint-disable-next-line no-underscore-dangle
    const payload = { email: user.email, role: user.role, userId: user._id };
    const jwtToken = jwt.sign(payload, process.env.JWTKEY);
    return res.status(200).json({ jwt_token: jwtToken });
  }
  return res.status(403).json({ message: 'Not authorized' });
};

const restoreUserPassword = async (req, res, next) => {
  const { email } = req.body;
  try {
    await sendPassword(email);
    res.status(200).json({ message: 'New password sent to your email address' });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  registerUser,
  loginUser,
  restoreUserPassword,
};
