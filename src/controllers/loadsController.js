require('dotenv').config();
const jwt = require('jsonwebtoken');
const { loadJoiSchema } = require('../models/Load');
const {
  saveLoad, findDriverLoads, findShipperLoads, findActiveLoad, findDriverLoad, findShipperLoad,
  putLoad, delLoad, setNextState, setNextStatus, addLog, setAssigned,
} = require('../services/loadsService');
const {
  findTrucksPD, changeTruckStatus, findActiveTruck, findShippingTruck,
} = require('../services/trucksService');

// eslint-disable-next-line consistent-return
async function createLoad(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const {
      // eslint-disable-next-line camelcase
      name, payload, pickup_address, delivery_address, dimensions,
    } = req.body;
    // eslint-disable-next-line camelcase
    const created_by = tokenPayload.userId;
    await loadJoiSchema.validateAsync({
      // eslint-disable-next-line camelcase
      name, payload, pickup_address, delivery_address, dimensions,
    });
    // eslint-disable-next-line no-unused-vars
    const load = await saveLoad({
      // eslint-disable-next-line camelcase
      created_by, name, payload, pickup_address, delivery_address, dimensions,
    });
    return await res.status(200).json({ message: 'Load created successfully' });
  } catch (err) {
    next(err);
  }
}

async function getLoads(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const id = tokenPayload.userId;
    const { role } = tokenPayload;
    if (role === 'DRIVER') {
      findDriverLoads(id).then((result) => {
        res.status(200).json({ loads: result });
      });
    } else if (role === 'SHIPPER') {
      findShipperLoads(id).then((result) => {
        res.status(200).json({ loads: result });
      });
    }
  } catch (err) {
    next(err);
  }
}

async function getActiveLoad(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const id = tokenPayload.userId;
    findActiveLoad(id).then((result) => {
      if (result) {
        res.status(200).json({ load: result });
      } else {
        res.status(400).json({ message: 'There is no active load' });
      }
    });
  } catch (err) {
    next(err);
  }
}

async function getLoad(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { id } = req.params;
    const { userId } = tokenPayload;
    const { role } = tokenPayload;
    if (role === 'DRIVER') {
      findDriverLoad(id, userId).then((result) => {
        res.status(200).json({ load: result });
      });
    } else if (role === 'SHIPPER') {
      findShipperLoad(id, userId).then((result) => {
        res.status(200).json({ load: result });
      });
    }
  } catch (err) {
    next(err);
  }
}

async function updateLoad(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { id } = req.params;
    const { userId } = tokenPayload;
    const {
      // eslint-disable-next-line camelcase
      name, payload, pickup_address, delivery_address, dimensions,
    } = req.body;
    const load = await findShipperLoad(id, userId);
    console.log(load.status);
    if (load.status === 'NEW') {
      putLoad({
        // eslint-disable-next-line camelcase
        id, name, payload, pickup_address, delivery_address, dimensions,
      }).then(() => {
        res.status(200).json({ message: 'Load details changed successfully' });
      });
    } else {
      res.status(400).json({ load: 'This is not NEW load' });
    }
  } catch (err) {
    next(err);
  }
}

// eslint-disable-next-line consistent-return
async function deleteLoad(req, res, next) {
  const { authorization } = req.headers;

  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { id } = req.params;
    const { userId } = tokenPayload;
    const load = await findShipperLoad(id, userId);
    console.log(load.status);
    if (load.status === 'NEW') {
      delLoad(id).then(() => {
        res.status(200).json({ message: 'Load deleted successfully' });
      });
    } else {
      res.status(400).json({ load: 'This is not NEW load' });
    }
  } catch (err) {
    next(err);
  }
}

async function iterateNextLoadState(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  const states = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to Delivery', 'Arrived to Delivery'];

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { userId } = tokenPayload;
    const load = await findActiveLoad(userId);
    if (load) {
      console.log('LOAD: ', load);
      if (load.state === null) {
        setNextState(userId, states[0]).then(() => {
          res.status(200).json({ message: `Load state changed to '${states[0]}'` });
        });
      } else if (load.state === 'En route to Delivery') {
        console.log('En route to Delivery');
        setNextState(userId, states[states.length - 1]).then(async () => {
          // eslint-disable-next-line no-underscore-dangle
          await setNextStatus(load._id, 'SHIPPED');
          await findActiveTruck(userId);
          res.status(200).json({ message: `Load state changed to '${states[states.length - 1]}'` });
        });
      } else {
        console.log(load.state);
        const i = states.indexOf(load.state);
        setNextState(userId, states[i + 1]).then(() => {
          res.status(200).json({ message: `Load state changed to '${states[i + 1]}'` });
        });
      }
    } else {
      res.status(400).json({ load: 'There is no assigned load' });
    }
  } catch (err) {
    next(err);
  }
}

async function postLoad(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { id } = req.params;
    const { userId } = tokenPayload;

    const load = await findShipperLoad(id, userId);
    // console.log('Load ', load);
    // console.log(load.status);
    await setNextStatus(id, 'POSTED');
    await addLog(id, 'Load posted', new Date().toISOString());
    findTrucksPD(load.payload, load.dimensions).then(async (result) => {
      console.log('FINDING TRUCK');
      console.log(result);
      if (result) {
        const truck = result[0];
        console.log(`Truck ${truck}`);
        // eslint-disable-next-line no-underscore-dangle
        await changeTruckStatus(truck._id, 'OL');
        await setNextStatus(id, 'ASSIGNED');
        console.log(`Assigned to ${truck.assigned_to}`);
        await setAssigned(id, truck.assigned_to);
        await setNextState(truck.assigned_to, 'En route to Pick Up');
        addLog(id, `Load assigned to driver with id ${truck.assigned_to}`, new Date().toISOString()).then(() => {
          res.status(200).json({
            message: 'Load posted successfully',
            driver_found: true,
          });
        });
      } else {
        await setNextStatus(id, 'NEW');
        await addLog(id, 'Load status rolled back to NEW', new Date().toISOString()).then(() => {
          res.status(400).json({ message: 'No trucks found' });
        });
      }
    });
  } catch (err) {
    next(err);
  }
}

async function getShippingInfo(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const { id } = req.params;
    const { userId } = tokenPayload;
    const load = await findShipperLoad(id, userId);
    console.log(load.assigned_to);
    findShippingTruck(load.assigned_to).then((result) => {
      res.status(200).json({ load, truck: result });
    });
  } catch (err) {
    next(err);
  }
}

module.exports = {
  createLoad,
  getLoads,
  getActiveLoad,
  getLoad,
  updateLoad,
  deleteLoad,
  iterateNextLoadState,
  postLoad,
  getShippingInfo,
};
