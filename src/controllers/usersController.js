require('dotenv').config();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { findUser, patchUser, delUser } = require('../services/usersService');

// eslint-disable-next-line consistent-return
function getUser(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    findUser(tokenPayload.userId)
      .then((user) => {
        res.status(200).json({ user });
      });
  } catch (err) {
    next(err);
  }
}

// eslint-disable-next-line consistent-return
function deleteUser(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    delUser(tokenPayload.userId)
      .then(() => {
        res.status(200).json({ message: 'Profile deleted successfully' });
      });
  } catch (err) {
    next(err);
  }
}

// eslint-disable-next-line consistent-return
async function changeUserPassword(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    const user = await findUser(tokenPayload.userId);
    const id = tokenPayload.userId;
    const { newPassword } = req.body;
    if (user && await bcrypt.compare(String(req.body.oldPassword), String(user.password))) {
      return patchUser(id, newPassword)
        .then(() => {
          res.status(200).json({ message: 'Password changed successfully' });
        });
    }
    return res.status(400).json({ message: 'Wrong password' });
  } catch (err) {
    next(err);
  }
}

module.exports = {
  getUser,
  deleteUser,
  changeUserPassword,
};
