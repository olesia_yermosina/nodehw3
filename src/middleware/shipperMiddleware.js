require('dotenv').config();
const jwt = require('jsonwebtoken');

// eslint-disable-next-line consistent-return
const shipperMiddleware = (req, res, next) => {
  const {
    authorization,
  } = req.headers;

  if (!authorization) {
    return res.status(401).json({ message: 'Please, provide authorization header' });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWTKEY);
    if (tokenPayload.role !== 'SHIPPER') {
      return res.status(400).json({ message: 'Available only for SHIPPER users' });
    }
    req.user = {
      userId: tokenPayload.userId,
      email: tokenPayload.email,
      role: tokenPayload.role,
    };
    next();
  } catch (err) {
    return res.status(401).json({ message: err.message });
  }
};

module.exports = {
  shipperMiddleware,
};
