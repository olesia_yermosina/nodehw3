const mongoose = require('mongoose');
const Joi = require('joi');

const loadJoiSchema = Joi.object({
  created_by: Joi.string(),

  assigned_to: Joi.string(),

  status: Joi.string()
    .pattern(/^(NEW|POSTED|ASSIGNED|SHIPPED)$/),

  state: Joi.string()
    .pattern(/^(En route to Pick Up|Arrived to Pick Up|En route to Delivery|Arrived to Delivery)$/),

  name: Joi.string()
    .required(),

  payload: Joi.number()
    .required(),

  pickup_address: Joi.string()
    .required(),

  delivery_address: Joi.string()
    .required(),

  dimensions: Joi.object({
    width: Joi.number(),
    length: Joi.number(),
    height: Joi.number(),
  }).required(),

  logs: Joi.array(),

});

const LoadSchema = mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
    default: 'NEW',
  },
  state: {
    type: String,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
  },
  logs: {
    type: Array,
  },
}, { timestamps: true });

const Load = mongoose.model('Load', LoadSchema);

module.exports = {
  Load,
  loadJoiSchema,
};
