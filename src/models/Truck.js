const mongoose = require('mongoose');
const Joi = require('joi');

const truckJoiSchema = Joi.object({
  created_by: Joi.string(),

  assigned_to: Joi.string(),

  type: Joi.string()
    .pattern(/^(SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT)$/)
    .required(),

  status: Joi.string(),
});

const TruckSchema = mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'IS',
  },
}, { timestamps: true });

const Truck = mongoose.model('Truck', TruckSchema);

module.exports = {
  Truck,
  truckJoiSchema,
};
