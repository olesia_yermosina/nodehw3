const { Load } = require('../models/Load');

const saveLoad = async ({
  // eslint-disable-next-line camelcase
  created_by, name, payload, pickup_address, delivery_address, dimensions,
}) => {
  const load = new Load({
    // eslint-disable-next-line camelcase
    created_by,
    name,
    payload,
    // eslint-disable-next-line camelcase
    pickup_address,
    // eslint-disable-next-line camelcase
    delivery_address,
    dimensions,
  });

  // eslint-disable-next-line no-return-await
  return await load.save();
};

// eslint-disable-next-line no-return-await
const findDriverLoads = async (id) => await Load.find({ assigned_to: id }, '-__v');

// eslint-disable-next-line no-return-await
const findShipperLoads = async (id) => await Load.find({ created_by: id }, '-__v');

// eslint-disable-next-line no-return-await
const findActiveLoad = async (id) => await Load.findOne({ assigned_to: id, status: { $nin: ['SHIPPED'] } }, '-__v');

// eslint-disable-next-line no-return-await
const findDriverLoad = async (id, userId) => await Load.findOne({ _id: id, assigned_to: userId }, '-__v');

// eslint-disable-next-line no-return-await
const findShipperLoad = async (id, userId) => await Load.findOne({ _id: id, created_by: userId }, '-__v');

const putLoad = async ({
  // eslint-disable-next-line camelcase
  id, name, payload, pickup_address, delivery_address, dimensions,
}) => {
  const load = await Load.findById(id);
  if (name) load.name = name;
  if (payload) load.payload = payload;
  // eslint-disable-next-line camelcase
  if (pickup_address) load.pickup_address = pickup_address;
  // eslint-disable-next-line camelcase
  if (delivery_address) load.delivery_address = delivery_address;
  if (dimensions) load.dimensions = dimensions;

  // eslint-disable-next-line no-return-await
  return await load.save();
};

// eslint-disable-next-line no-return-await
const delLoad = async (id) => await Load.findByIdAndDelete(id);

const setNextState = async (userId, state) => {
  const load = await Load.findOneAndUpdate({ assigned_to: userId, status: { $nin: ['SHIPPED'] } }, {
    $set:
        { state },
  });
  console.log(load);
  // eslint-disable-next-line no-return-await
  return await load.save();
};

const setNextStatus = async (id, status) => {
  const load = await Load.findById(id);
  load.status = status;
  // eslint-disable-next-line no-return-await
  return await load.save();
};

const addLog = async (id, message, time) => {
  const load = await Load.findById(id);
  const { logs } = load;
  logs.push({
    message,
    time,
  });
  // eslint-disable-next-line no-return-await
  return await load.save();
};

const setAssigned = async (id, userId) => {
  const load = await Load.findByIdAndUpdate(id, {
    $set:
        { assigned_to: userId },
  });
  // eslint-disable-next-line no-return-await
  return await load.save();
};

module.exports = {
  saveLoad,
  findDriverLoads,
  findShipperLoads,
  findActiveLoad,
  findDriverLoad,
  findShipperLoad,
  putLoad,
  delLoad,
  setNextState,
  setNextStatus,
  addLog,
  setAssigned,
};
