const { Truck } = require('../models/Truck');

// eslint-disable-next-line camelcase
const saveTruck = async ({ created_by, type }) => {
  const truck = new Truck({
    // eslint-disable-next-line camelcase
    created_by,
    type,
  });

  // eslint-disable-next-line no-return-await
  return await truck.save();
};

// eslint-disable-next-line no-return-await
const findTrucks = async (id) => await Truck.find({ created_by: id }, '-__v');

// eslint-disable-next-line no-return-await
const findTruck = async (id) => await Truck.findById(id);

// eslint-disable-next-line no-return-await
const findActiveTruck = async (userId) => await Truck.findOneAndUpdate({ assigned_to: userId, status: 'OL' }, {
  $set:
      { status: 'IS' },
});

// eslint-disable-next-line no-return-await
const findShippingTruck = async (userId) => await Truck.findOne({ assigned_to: userId, status: 'OL' });

const putTruck = async (id, type) => {
  const truck = await Truck.findById(id);
  truck.type = type;
  // eslint-disable-next-line no-return-await
  return await truck.save();
};

// eslint-disable-next-line no-return-await
const delTruck = async (id) => await Truck.findByIdAndDelete(id);

// eslint-disable-next-line no-return-await
const isHasTruck = async (userId) => await Truck.findOne({ assigned_to: userId });

const assignTruckTo = async (id, userId) => {
  const truck = await Truck.findByIdAndUpdate(id, {
    $set:
        { assigned_to: userId },
  });
  // eslint-disable-next-line no-return-await
  return await truck.save();
};

// eslint-disable-next-line consistent-return
const findTrucksPD = async (payload, dim) => {
  if (payload <= 1700 && dim.width <= 300 && dim.length <= 250 && dim.height <= 170) {
    // eslint-disable-next-line no-return-await
    return await Truck.find({ assigned_to: { $nin: [null] }, status: 'IS' }, '-__v');
  } if (payload <= 2500 && dim.width <= 500 && dim.length <= 250 && dim.height <= 170) {
    // eslint-disable-next-line no-return-await
    return await Truck.find({ assigned_to: { $nin: [null] }, status: 'IS', type: { $in: ['SMALL STRAIGHT', 'LARGE STRAIGHT'] } }, '-__v');
  } if (payload <= 4000 && dim.width <= 700 && dim.length <= 350 && dim.height <= 200) {
    // eslint-disable-next-line no-return-await
    return await Truck.find({ assigned_to: { $nin: [null] }, status: 'IS', type: 'LARGE STRAIGHT' }, '-__v');
  }
};

const changeTruckStatus = async (id, status) => {
  const truck = await Truck.findById(id);
  truck.status = status;
  // eslint-disable-next-line no-return-await
  return await truck.save();
};

module.exports = {
  saveTruck,
  findTrucks,
  findTruck,
  putTruck,
  delTruck,
  isHasTruck,
  assignTruckTo,
  findTrucksPD,
  changeTruckStatus,
  findActiveTruck,
  findShippingTruck,
};
