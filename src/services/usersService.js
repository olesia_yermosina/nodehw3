const bcrypt = require('bcryptjs');
const { User } = require('../models/User');

// eslint-disable-next-line no-return-await
const findUser = async (id) => await User.findById(id);

// eslint-disable-next-line no-return-await
const patchUser = async (id, newPassword) => await User.findByIdAndUpdate(
  { _id: id },
  { $set: { password: await bcrypt.hash(newPassword, 10) } },
);

// eslint-disable-next-line no-return-await
const delUser = async (id) => await User.findByIdAndDelete(id);

module.exports = {
  findUser,
  patchUser,
  delUser,
};
