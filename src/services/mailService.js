require('dotenv').config();
const nodemailer = require('nodemailer');

async function sendPassword(reciever) {
  const mailTransporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD,
    },
  });

  const mailDetails = {
    from: 'hw3passrestore@gmail.com',
    to: reciever,
    subject: 'Password restore',
    text: `${reciever.slice(0, 5)}123pass`,
  };

  await mailTransporter.sendMail(mailDetails);
}

module.exports = {
  sendPassword,
};
