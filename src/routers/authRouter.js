const express = require('express');

const router = express.Router();
const {
  registerUser, loginUser, restoreUserPassword,
} = require('../controllers/authController');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.post('/register', asyncWrapper(registerUser));

router.post('/login', loginUser);

router.post('/forgot_password', restoreUserPassword);

module.exports = {
  authRouter: router,
};
