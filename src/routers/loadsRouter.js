const express = require('express');

const router = express.Router();

const {
  createLoad, getLoads, getActiveLoad, getLoad, updateLoad, deleteLoad, iterateNextLoadState,
  postLoad, getShippingInfo,
} = require('../controllers/loadsController');

const { driverMiddleware } = require('../middleware/driverMiddleware');

const { shipperMiddleware } = require('../middleware/shipperMiddleware');

const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/', shipperMiddleware, createLoad);

router.get('/', authMiddleware, getLoads);

router.get('/active', driverMiddleware, getActiveLoad);

router.get('/:id', authMiddleware, getLoad);

router.put('/:id', shipperMiddleware, updateLoad);

router.delete('/:id', shipperMiddleware, deleteLoad);

router.patch('/active/state', driverMiddleware, iterateNextLoadState);

router.post('/:id/post', shipperMiddleware, postLoad);

router.get('/:id/shipping_info', shipperMiddleware, getShippingInfo);

module.exports = {
  loadsRouter: router,
};
