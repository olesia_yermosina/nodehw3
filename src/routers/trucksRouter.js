const express = require('express');

const router = express.Router();
const {
  createTruck, getTrucks, getTruck, updateTruck, deleteTruck, assignTruck,
} = require('../controllers/trucksController');
const { driverMiddleware } = require('../middleware/driverMiddleware');

router.post('/', driverMiddleware, createTruck);

router.get('/', driverMiddleware, getTrucks);

router.get('/:id', driverMiddleware, getTruck);

router.put('/:id', driverMiddleware, updateTruck);

router.delete('/:id', driverMiddleware, deleteTruck);

router.post('/:id/assign', driverMiddleware, assignTruck);

module.exports = {
  trucksRouter: router,
};
