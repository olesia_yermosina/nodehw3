const express = require('express');

const router = express.Router();
const { getUser, deleteUser, changeUserPassword } = require('../controllers/usersController');
const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/me', authMiddleware, getUser);

router.patch('/me/password', authMiddleware, changeUserPassword);

router.delete('/me', authMiddleware, deleteUser);

module.exports = {
  usersRouter: router,
};
